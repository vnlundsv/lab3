package com.example.vetle.lab3;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.SensorEvent;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

/**
 * View class draws the view presented to user and contains balls movelogic
 */
public class SceneBall extends View {
    private Paint paint;
    Context context;
    MediaPlayer sound;

    //Ball variables
    private int ballX;
    private int ballY;
    private final int ballRadius = 20;
    private Paint ballPaint;

    //Move variables
    private int xUpperBoundary;
    private int yUpperBoundary;
    private int bothLowerBoundary;
    private final int step = 4;
    private final int pushBack = 20;

    //Dimensions
    private int width;
    private int height;
    private final int margin = 40;
    private final int stroke = 30;

    //Fence
    private Rect fence;
    private Paint fencePaint;

    private Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

    /**
     * Constructor, initialize graphics and sound
     *
     * @param context Context
     */
    public SceneBall(Context context) {
        super(context);
        this.context = context;

        fetchWindowDimensions();

        //Fence setup
        fence = new Rect(margin, margin, width - margin, height - margin);
        fencePaint = new Paint();
        fencePaint.setColor(Color.BLACK);
        fencePaint.setStrokeWidth(stroke);
        fencePaint.setStyle(Paint.Style.STROKE);
        xUpperBoundary = width - margin - stroke;
        yUpperBoundary = height - margin - stroke;
        bothLowerBoundary = 0 + margin + stroke;

        //Ball setup
        ballX = width / 2;
        ballY = height / 2;
        ballPaint = new Paint();
        ballPaint.setColor(Color.BLUE);

        //Sound setup
        sound = MediaPlayer.create(context, notification);
    }

    /**
     * https://stackoverflow.com/a/1016941/7036624
     */
    private void fetchWindowDimensions() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
    }

    /**
     * Overrided draw function. Draws fence + ball continually to enable animation through invalidate()
     *
     * @param canvas Canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(fence, fencePaint);
        canvas.drawCircle(ballX, ballY, ballRadius, ballPaint);
        invalidate();
    }

    /**
     * Move logic for the ball.
     * If ball collides with boundries - move ball and play sound + vibrate else move ball
     *
     * @param event SensorEvent
     */
    public void moveBall(SensorEvent event) {
        int xCheck = ballX + (int) event.values[1];
        int yCheck = ballY + (int) event.values[0];

        //X axis logic
        if (xCheck >= xUpperBoundary) {
            vibrate();
            ping();
            for (int i = 0; i <= pushBack; i++){
                ballX = ballX - i;
            }

        } else if (xCheck <= bothLowerBoundary) {
            vibrate();
            ping();
            for (int i = 0; i <= pushBack; i++){
                ballX = ballX - i;
            }
        } else {
            ballX = (int) (ballX + step * (0.5 * event.values[1]));
        }

        //Y axis logic
        if (yCheck >= yUpperBoundary) {
            vibrate();
            ping();
            for (int i = 0; i <= pushBack; i++){
                ballY = ballY - i;
            }

        } else if (yCheck <= bothLowerBoundary) {
            vibrate();
            ping();
            for (int i = 0; i <= pushBack; i++){
                ballY = ballY + i;
            }
        } else {
            ballY = (int) (ballY + step * (0.5 * event.values[0]));
        }

    }

    /**
     * https://stackoverflow.com/questions/13950338/how-to-make-an-android-device-vibrate
     */
    private void vibrate() {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        v.vibrate(100);
    }

    /**
     * https://stackoverflow.com/questions/18459122/play-sound-on-button-click-android
     */

    private void ping() {
        try {
            if (sound.isPlaying()) {
                sound.stop();
                sound.release();
                sound = MediaPlayer.create(context, notification);
            }
            sound.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
